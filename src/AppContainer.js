import React, { Component } from 'react';
import { createSwitchNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import store from './store/store';
import ChatScreen from './components/screens/ChatScreen';
import LoginScreen from './components/screens/LoginScreen';
import SearchScreen from './components/screens/SearchScreen';

const MessageStack = createStackNavigator(
    {
        ChatScreen: ChatScreen,
        search:SearchScreen
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: "rgb(162,55,243)"
            }
        }
    }
)
// const searchStack =createStackNavigator(
//     {
//         search:SeachSceen
//     }
// )

const Switch = createSwitchNavigator({
    main: MessageStack,
    login: LoginScreen,
    
},
    {
        initialRouteName: 'login'
    }
)

const Container = createAppContainer(Switch);

class AppContainer extends Component {

    render() {
   
        
        return (
            <Provider store={store}>
                <Container />
            </Provider>
        )
    }
}

export default AppContainer;
