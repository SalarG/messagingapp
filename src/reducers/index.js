import { combineReducers } from 'redux';
import ChatReducer from './ChatReducer';
import AuthReducer from './AuthReducer';
import received_messages from './received_messages';
//import SearchReducer from './SearchReducer';

export default combineReducers({
   ChatReducer,
   AuthReducer,
   received_messages,
  //SearchReducer
   //banana: () => []
});
