import { TEXT_SENT } from '../actions/types';



const INITIAL_STATE = {
    messageText: '',
    messages: [
        {
            fromUser: "ُSalar",
            message: "Hey Man",
            dateTime:"14 - 4 \n"+"18:09"
        },
        {
            fromUser: "Salar",
            message: "Hope You Are Doing Well",
            dateTime: "14 - 4 \n"+"18:09"
        },
        {
            fromUser: "Salar",
            message: "This is just a mock-up of a messaging app. you can type in below and tap send to see your message here. For the next step I can add backend(e.g. firebase) to this app so multiple devices can communicate to each other",
            dateTime: "14 - 4 \n"+"18:09"
        }

    ]
}
export default function (state = INITIAL_STATE, action) {
    //console.log('insideReducer',action.payload)

    switch (action.type) {
        case TEXT_SENT:
            return {
                messages: [...state.messages, action.payload]
            }
        default:
            return state;
    }
}