import { NAME_SENT } from '../actions/types';

const INITIAL_STATE = {
    userName: ''
}
export default function (state = INITIAL_STATE, action) {
    //console.log('insideReducer',action)
    switch (action.type) {
        case NAME_SENT:
            return { ...state, userName: action.payload }
        default:
            return state;
    }
}