import React, { Component } from 'react';
import {
  Dimensions,
  Text,
  View,
  TextInput,
  FlatList,
  StatusBar,
  Keyboard,
  Platform
} from 'react-native';
import { Button, SearchBar, Icon } from 'react-native-elements'
import { connect } from 'react-redux';
import Message from '../screens/view/Message';
import Card from '../elements/Card';
import { textSent} from '../../actions/index';


class ChatScreen extends Component {

  constructor() {
    super();

    this.state = {
      inputHeight: 10,
      messageText: '',

    }
  }
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Messages",
      headerTitleStyle: {
        fontWeight: 'bold',
        color: "rgb(255,255,255)",
        textAlign: "center",
        flex: 1
      }, headerLeft:
        <Button
          color='#848886'
          title= 'search'
          onPress= {navigation.getParam('search')}
        />,
      headerRight: <Button
        title="Logout"
        onPress={navigation.getParam('logOut')}
        backgroundColor="rgba(0,0,0,0)"
        color="rgba(0, 122, 255, 1)"
      />
    }
  }

  logOut = () => {
    this.props.navigation.navigate('login');
  }

  search = ()=>{
    this.props.navigation.navigate('search');
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();

  }

  componentDidMount() {
    this.props.navigation.setParams({ logOut: this.logOut });
    this.props.navigation.setParams({ search: this.search });

    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
  }



  _keyboardDidShow = (e) => {
    this.setState({ inputHeight: e.endCoordinates.height+30 });

  }

  _keyboardDidHide = () => {
    this.setState({ inputHeight: 30 });

  }

  onChangeText = (text) => {
    this.setState({ messageText: text })
    
  }

  onPressSend = () => {
    var currentDate = new Date();

    var date = currentDate.getDate();
    var month = currentDate.getMonth(); 


    var hour = currentDate.getHours();
    var minutes = currentDate.getMinutes()
    var dateString = date + " - " + (month + 1) + " \n" + hour + ":" + minutes;

    const { messageText } = this.state;

    if (messageText.length > 0) {

      let message = {
        message: messageText,
        fromUser: this.props.userName,
        //dateTime: new Date()
        dateTime: dateString
      }
      this.props.textSent(message);
      this.setState({ messageText: '' });
    }

  }
  renderRow = ({ item }) => {
    return (
      <Message
        {...item}
        userName={this.props.userName}
      >
      </Message>
    )
  }
  
  render() {
    let { height, Width } = Dimensions.get('window');

    return (
      <View>
       
        <Card style={styles.cardStyle}>
          <View style={styles.container}>
            <StatusBar barStyle="light-content" />
            <FlatList
              style={{ padding: 20, height: height * 0.7 }}
              data={this.props.messages}
              renderItem={this.renderRow}
              keyExtractor={item => item.id}
            />
          </View>
          <View style={[styles.textContainer, { bottom: this.state.inputHeight }]}>
            <TextInput style={
              styles.textInput
            }
              autoCapitalize={'sentences'}
              autoCorrect={true}
              //autoFocus={true}
              placeholder={'Type Here'}
              onChangeText={this.onChangeText}
              value={this.state.messageText}
            />
            <Button
              style={styles.button}
              title="Send"
              //raised="true"
              onPress={this.onPressSend} />
          </View>
        </Card>
   
      </View>
    );
  }
}

mapStateToProps = (state) => {
  //console.log('################################################state', state);
  const { searchText, messages } = state.ChatReducer;
  const { userName } = state.AuthReducer;
  return {
    searchText,
    messages,
    userName,
    
  }

};

export default connect(mapStateToProps, { textSent })(ChatScreen);

const styles = {

  container: {
    width: 100 + "%",
    height: 100 + "%",
    display: "flex",
    //flex: 1,
    justifyContent: "center",
    alignContent: "center",
    paddingBottom:40
  },
  cardStyle:{
    paddingBottom:50
  },
  textInput: {
    marginBottom: 5,
    flex: 3,
    backgroundColor: '#C5C8C6'
  },
  button: {
    flex: 1,
    paddingBottom: 15,
    marginLeft: 5
  },
  textContainer: {
    flexDirection: 'row',
    //flex:1,
    justifyContent: 'flex-end',
    marginBottom: 20
  }
}