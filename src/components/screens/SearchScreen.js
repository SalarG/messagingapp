import React, { Component } from 'react';
import { View } from 'react-native';
import SearchableDropdown from 'react-native-searchable-dropdown';
import { searchMessages } from '../../actions';
import { connect } from 'react-redux';

class SearchScreen extends Component {

  
    render() {

        let messages = this.props.messages;

        var messageArray = [];

        messages.forEach(e => {
            messageArray.push({name: e.message})
        });
        console.log('messageArray', messageArray);

        return (
            <View style={styles.container}>
                <SearchableDropdown
                    onTextChange={text => console.log(text)}
                    onItemSelect={item => alert('item was found')}
                    containerStyle={{ padding: 5 }}
                    placeholder="Search Here"
                    containerStyle={{ padding: 5,  }}
                    //suggestion container style
                    textInputStyle={{
                        //inserted text style
                        padding: 12,
                        borderWidth: 1,
                        borderColor: '#ccc',
                        backgroundColor: '#FAF7F6',
                    }}
                    itemStyle={{
                        //single dropdown item style
                        padding: 10,
                        marginTop: 2,
                        backgroundColor: '#FAF9F8',
                        borderColor: '#bbb',
                        borderWidth: 1,
                    }}
                    itemTextStyle={{
                        //single dropdown item's text style
                        color: '#222',
                    }}
                    itemsContainerStyle={{
                        //items container style you can pass maxHeight
                        //to restrict the items dropdown hieght
                        maxHeight: 640,
                    }}
                    items={messageArray}
                    placeholder="Search Here"
                />
            </View>
        )
    }
}

mapStateToProps = (state) => {
    //console.log('state', state);

    return {
        //searchText: state.SearchReducer.searchText,
        //foundResult: state.SearchReducer.foundResult
        messages: state.ChatReducer.messages
    }
}
export default connect(mapStateToProps, { searchMessages })(SearchScreen);

const styles = {

    container: {
        marginTop: '0.1%',
        display: "flex",
        //flex: 1,
        justifyContent: "center",
        alignContent: "center"
    }
}