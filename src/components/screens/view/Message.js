import React from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';


class Message extends React.Component {


    render() {
        //console.log('date' , this.props.dateTime);

        if (this.props.userName !== this.props.fromUser) {
            return (
                <View
                    style={[{
                        alignSelf: 'flex-start',
                        backgroundColor: '#c4e5f6'
                    }, styles.container]}>
                    <Text style={styles.fromUserRecieved}>{this.props.fromUser}</Text>
                    <Text style={styles.dateRecieved}>{this.props.dateTime}</Text>
                    <Text style={styles.messageTextRecieved}>{this.props.message}</Text>
                </View>
            )
        } else {
            return (
                <View
                    style={[{
                        alignSelf: 'flex-end',
                        justifyContent:'flex-start',
                        alignItems: 'flex-end',
                        backgroundColor: '#fdceb3'
                    }, styles.container]}>
                    
                    <Text style={styles.fromUserSent}>{this.props.fromUser}</Text>
                    <Text style={styles.dateSent}>{this.props.dateTime}</Text>
                    <Text style={styles.messageTextSent}>{this.props.message}</Text>
                    
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        width: '80%',
        borderRadius: 5,
        marginBottom: 10
    },
    fromUserRecieved: {
        padding: 1,
        fontSize: 16,
        color: '#000000',
    },
    messageTextRecieved: {
        padding: 1,
        fontSize: 14,
        color: '#000000',
        flex:1
    },
    dateRecieved: {
        color: "rgb(102,102,102)",
        
    },
    fromUserSent: {
        padding: 1,
        fontSize: 16,
        color: '#000000',
        
    },
    messageTextSent: {
        padding: 10,
        fontSize: 14,
        color: '#000000',
        paddingLeft: 5,
   
    },
    dateSent: {
        color: "rgb(102,102,102)"
    },
    
});


export default Message;