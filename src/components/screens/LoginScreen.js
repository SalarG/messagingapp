import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Alert } from 'react-native'
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { nameSent } from '../../actions/index'

class LoginScreen extends Component {

    constructor() {
        super();
        this.state = {
            name: ''
        }
    }

    submitForm = () => {
        if (this.state.name.length < 3) {
            Alert.alert('Error', 'Name is not Valid')
        } else {
            this.props.nameSent(this.state.name);
            this.setState({ name: '' });
            this.props.navigation.navigate('main')
        }
    }

    onChangeText = (text) => {
        this.setState({ name: text });
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    style={styles.input}
                    placeholder={'Name'}
                    onChangeText={this.onChangeText}
                    value={this.state.name}
                />
                <Button
                    buttonStyle={styles.btn}
                    titleStyle={styles.btnTitle}
                    title='Enter'
                    onPress={this.submitForm} />
            </View>
        )
    }
}


// mapStateToProps = state =>{
//     //console.log('state', state.AuthReducer.name);
//     return {name:state.name}
// }

export default connect(null, { nameSent })(LoginScreen);


const styles =StyleSheet.create( {
    container: {
        //width: 100 + "%",
        //height: 100 + "%",
        //display: "flex",
        flex: 1,
        flexDirection:'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#D4DED9'
    },
    input: {
        padding: 15,
        backgroundColor: '#FAFFFC',
        width: '85%',
        marginBottom: 10,
        marginTop:55,
        borderRadius: 5
    },
    btn: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        marginTop: 5
    },
    btnTitle: {
        //alignSelf: 'center',
        //justifyContent:'center', 
        color: 'red',
        marginHorizontal: '20%'
    }

});