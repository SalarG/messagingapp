import {
    //TEXT_CHANGED,
    TEXT_SENT
} from './types';

// export const textChanged = (text) => {
//     return {
//         type: TEXT_CHANGED,
//         payload: text
//     };
//     //console.log('action',text);
// }

export const textSent = (text) => {
    return {
        type: TEXT_SENT,
        payload: text
    };
}
